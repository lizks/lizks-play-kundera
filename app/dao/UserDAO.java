package dao;


import com.impetus.client.cassandra.common.CassandraConstants;
import models.User;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 10/28/2015.
 */
public class UserDAO {

    public void create(User userInfo) {
        User user = new User();
        user.setUserId(userInfo.getUserId());
        user.setFirstName(userInfo.getFirstName());
        user.setLastName(userInfo.getLastName());
        user.setCity(userInfo.getCity());
        Map<String, String> propertyMap = new HashMap<String, String>();
        propertyMap.put(CassandraConstants.CQL_VERSION, CassandraConstants.CQL_VERSION_3_0);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("cassandra_pu", propertyMap);
        EntityManager em = emf.createEntityManager();

        try {
            em.setFlushMode(FlushModeType.COMMIT);   //Necessary for flusing on transaction commit, AUTO option flushes automatically
            em.getTransaction().begin();
            em.persist(user);
            // em.persist(role);
            em.getTransaction().commit();  //Will write p1, p2, p3 to database
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        }

        em.close();
        emf.close();
    }
}
