package controllers

import javax.inject.Inject

import com.google.gson.Gson
import dao.UserDAO
import play.api._
import play.api.mvc._
import utilities.Utils

class Application  @Inject()(userDAO: UserDAO) extends Controller {

  def create() = Action(BodyParsers.parse.json) {
    request =>
      println(request.body)
      val  gson = new Gson();
      val json=request.body
      val user = Utils.getUserFromJson(json.toString())
      userDAO.create(user)
      println(BodyParsers.parse.json)
     Ok("OK")
  }

}
