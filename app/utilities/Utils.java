package utilities;

import com.google.gson.Gson;

import models.User;


/**
 * Created by Administrator on 10/28/2015.
 */
public class Utils {


    public static User getUserFromJson(String json) {
        Gson gson = new Gson();
        User user = gson.fromJson(json, User.class);
        return user;
    }
}
